import asyncio
import pyaudio
import numpy as np
import threading
from time import perf_counter as time
from aioconsole import ainput

def main(*, host: str = '0.0.0.0', port: int = 12345):
	loop = asyncio.get_event_loop()
	server = Server(loop, host, port)
	loop.run_forever()

class Server:
	def __init__(self, loop, host, port):
		self.loop = loop
		self.queue = asyncio.Queue()
		self.connected = None
		self.tone_on = False
		self.tone_on_prev = False
		self.pressed = False
		self.pressed_time = None
		self.frame = 0
		
		asyncio.ensure_future(loop.create_server(self.on_open, host, port))
		asyncio.ensure_future(self._text_input())
		asyncio.ensure_future(self._queue_processor())
		asyncio.ensure_future(self._audio_player())
		threading.Thread(target = self._monitor_keyboard).start()
	
	def on_open(self):
		return Protocol(self)
	
	def try_call(self, ls):
		if self.connected is None:
			self.print_status("Call start.")
			self.connected = ls
		return self.connected is ls
	
	def message_received(self, ls, msg):
		if ls is not self.connected:
			ls.close()
			return
		self.print_status("Message:", msg)
		try:
			ms = min(int(msg) / 1000, 0.5)
		except ValueError:
			return
		self.queue.put_nowait(ms)
	
	async def _queue_processor(self):
		while self.loop.is_running():
			ms = await self.queue.get()
			self.tone_on = True
			await asyncio.sleep(ms)
			self.tone_on = False
	
	async def _audio_player(self):
		p = pyaudio.PyAudio()
		s = p.open(
			format = p.get_format_from_width(2), rate = 44000,
			channels = 1, output = True, stream_callback = self._audio_callback,
		)
		s.start_stream()
	
	async def _text_input(self):
		while self.loop.is_running():
			txt = (await ainput('> ')).strip()
			if not txt:
				continue
			cmd, *args = txt.split()
			if cmd in ('q', 'quit', 'exit'):
				self.print_status("Quitting")
				self.loop.stop()
				break
			if cmd in ('c', 'call'):
				if not args:
					args = ('127.0.0.1',)
				host = args[0]
				if len(args) < 2:
					port = 12345
				else:
					port = int(args[1])
				self.print_status("Calling", host, port)
				asyncio.ensure_future(self.loop.create_connection(self._make_call, host, port))
	
	def _monitor_keyboard(self):
		import keyboard
		def hook(event):
			new_pressed = (event.event_type != keyboard.KEY_UP)
			if new_pressed != self.pressed:
				if new_pressed:
					self.pressed_time = time()
				else:
					ms = int(1000 * (time() - self.pressed_time))
					self.pressed_time = None
					if self.connected:
						self.connected.write_message('m:{}'.format(ms))
				self.pressed = new_pressed
		keyboard.hook_key('`', hook)
		keyboard.wait()
	
	def _make_call(self):
		return CallerProtocol(self)
	
	def _audio_callback(self, in_data, frame_count, time_info, status):
		data = np.arange(self.frame, self.frame + frame_count, dtype = np.float32)
		data *= 0.1
		np.sin(data, out = data)
		
		tone_on = (self.tone_on or self.pressed)
		
		if tone_on:
			if not self.tone_on_prev:
				data *= np.linspace(0, 1, frame_count, dtype = np.float32)
		else:
			if self.tone_on_prev:
				data *= np.linspace(1, 0, frame_count, dtype = np.float32)
			else:
				data[:] = 0
		self.frame += frame_count
		self.tone_on_prev = tone_on
		
		return ((data * 32000).astype(np.int16), pyaudio.paContinue)
	
	def leave(self, ls):
		if self.connected is ls:
			self.print_status("Call end.")
			self.connected = None
		self.tone_on = False
	
	def print_status(self, *args, **kwargs):
		print(*args, **kwargs)

class BaseState:
	# Represents the state of an `asyncio.Protocol` while connected
	
	def __init__(self, server, transport):
		self.server = server
		self.transport = transport
		self.buffer = b''
	
	def data_received(self, data):
		buf = self.buffer
		
		if len(buf) >= 1000:
			buf = buf[-100:]
		buf += data
		
		try:
			while True:
				i = buf.find(b'\n')
				if i < 0:
					break
				msg = buf[:i]
				buf = buf[i+1:]
				if msg:
					self.message_received(msg.decode('utf-8').strip())
		finally:
			self.buffer = buf
	
	def write_message(self, msg):
		self.transport.write((msg + '\n').encode('utf-8'))
	
	def close(self):
		self.transport.close()

class CallerState(BaseState):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.write_message('call')
	
	def message_received(self, msg):
		if msg == 'accept':
			self.server.print_status("Accepted.")
			assert self.server.try_call(self)
			return
		if msg.startswith('m:'):
			self.server.message_received(self, msg[2:])
			return
		self.server.print_status("Busy.")
		self.close()

class CallerProtocol(asyncio.Protocol):
	def __init__(self, server):
		super().__init__()
		self.server = server
		self.state = None
	
	def connection_made(self, transport):
		self.state = CallerState(self.server, transport)
	
	def connection_lost(self, exc):
		self.server.leave(self.state)
		self.state = None
	
	def data_received(self, data):
		self.state.data_received(data)

class ListenerState(BaseState):
	def message_received(self, msg):
		if msg in ('c', 'call'):
			if self.server.try_call(self):
				self.write_message('accept')
			else:
				self.write_message('busy')
				self.close()
		elif msg in ('q', 'quit', 'exit'):
			self.close()
		elif msg.startswith('m:'):
			self.server.message_received(self, msg[2:])
		else:
			self.write_message('ignore')

class Protocol(asyncio.Protocol):
	def __init__(self, server):
		super().__init__()
		self.server = server
		self.state = None
	
	def connection_made(self, transport):
		self.state = ListenerState(self.server, transport)
	
	def connection_lost(self, exc):
		self.server.leave(self.state)
		self.state = None
	
	def data_received(self, data):
		self.state.data_received(data)

if __name__ == '__main__':
	import funcli
	funcli.main()
